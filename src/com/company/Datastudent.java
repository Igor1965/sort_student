package com.company;

public class Datastudent {
    String id;
    String name;
    String secondname;
    String mark;

    public Datastudent() {
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }




    public Datastudent(String id, String name, String secondname, String mark) {
        this.id = id;
        this.name = name;
        this.secondname = secondname;
        this.mark = mark;
    }
}
